# Templates
In deze map staan patronen voor queries die hergebruikt kunnen worden waar nodig.
## Contractomvang.rq
Patroon om om te gaan met contractomvang eigenschappen die gebruik maken van verschillende eenheden (fte in de varianten 40 uur/week en 36 uur/week, of direct uren per week). In de query een voorbeeld hoe de verschillende eenheden kunnen worden omgerekend naar een rapportage eenheid (als BIND variabele zelf te kiezen in het begin van de query).