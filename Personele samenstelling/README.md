# Indicator 2.1.1 Aantal Personeelsleden
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 2.1.2 Aantal fte
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Gewerkte periode](http://purl.org/ozo/onz-pers#GewerktePeriode)
- [Uitvoering van arbeid tijdseenheid](http://purl.org/ozo/onz-g#UnitOfWorkload)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
## Eigenschappen
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [heeftBeginTijd](http://purl.org/ozo/onz-g#hasBeginTimeStamp)
- [heeftEindTijd](http://purl.org/ozo/onz-g#hasEndTimeStamp)
## Instanties
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 2.1.3 Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Arbeidsovereenkomst bepaalde tijd](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomstBepaaldeTijd)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 2.1.4 Percentage inzet uitzendkrachten Personeel Niet in Loondienst (PNIL)
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [heeftBeginTijd](http://purl.org/ozo/onz-g#hasBeginTimeStamp)
- [heeftEindTijd](http://purl.org/ozo/onz-g#hasEndTimeStamp)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 2.1.5 Kosten Personeel
## Concepten
- [Grootboekpost](http://purl.org/ozo/onz-fin#Grootboekpost)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
## Eigenschappen
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [heeft datum](http://purl.org/ozo/onz-g#hasDate)
## Instanties
- [Lasten uit hoofde van personeelsbeloningen](http://purl.org/ozo/onz-fin#WPer)
- [Uitzendkrachten](http://purl.org/ozo/onz-fin#WBedOvpUik)
# Indicator 2.1.6 Gemiddelde contractomvang
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Gewerkte periode](http://purl.org/ozo/onz-pers#GewerktePeriode)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [heeftBeginTijd](http://purl.org/ozo/onz-g#hasBeginTimeStamp)
- [heeftEindTijd](http://purl.org/ozo/onz-g#hasEndTimeStamp)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 2.2.1 Percentage fte per niveau
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Gewerkte periode](http://purl.org/ozo/onz-pers#GewerktePeriode)
- [ODB Kwalificatiewaarde](http://purl.org/ozo/onz-pers#ODBKwalificatieWaarde)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [heeftBeginTijd](http://purl.org/ozo/onz-g#hasBeginTimeStamp)
- [heeftEindTijd](http://purl.org/ozo/onz-g#hasEndTimeStamp)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 2.2.2 Aantal Stagiairs
## Concepten
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Stage-overeenkomst](http://purl.org/ozo/onz-pers#StageOvereenkomst)
## Relaties
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 2.2.3 Aantal vrijwilligers
## Concepten
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Vrijwilligersovereenkomst](http://purl.org/ozo/onz-pers#VrijwilligersOvereenkomst)
## Relaties
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 2.3.1 Ziekteverzuimpercentage
## Concepten
- [Arbeidsongeschiktheid](http://purl.org/ozo/onz-pers#ArbeidsOngeschiktheid)
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Contractomvang](http://purl.org/ozo/onz-pers#ContractOmvang)
- [Ziekteperiode](http://purl.org/ozo/onz-pers#ZiektePeriode)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deelnemer](http://purl.org/ozo/onz-g#hasParticipant)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [heeft teller kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasNumeratorQualityValue)
- [is eigenschap of kenmerk van](http://purl.org/ozo/onz-g#inheresIn)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 2.3.2 Verzuimfrequentie
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Verzuimperiode](http://purl.org/ozo/onz-pers#VerzuimPeriode)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [is participant in](http://purl.org/ozo/onz-g#isParticipantIn)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 2.4.1 Percentage instroom
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 2.4.2 Percentage uitstroom
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 2.4.3 Percentage doorstroom
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [ODB Kwalificatiewaarde](http://purl.org/ozo/onz-pers#ODBKwalificatieWaarde)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 2.5.1 Aantal fte zorg client ratio
## Concepten
- [Verpleegproces](http://purl.org/ozo/onz-zorg#NursingProcess)
- [Wlz-indicatie](http://purl.org/ozo/onz-zorg#WlzIndicatie)
## Relaties
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [VV Beschermd verblijf met intensieve palliatief-terminale zorg](http://purl.org/ozo/onz-zorg#10VV)
- [VV Beschermd wonen met intensieve dementiezorg](http://purl.org/ozo/onz-zorg#5VV)
- [VV Beschermd wonen met intensieve verzorging en verpleging](http://purl.org/ozo/onz-zorg#6VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding](http://purl.org/ozo/onz-zorg#7VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging](http://purl.org/ozo/onz-zorg#8VV)
- [VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging](http://purl.org/ozo/onz-zorg#4VV)
- [VV Herstelgerichte behandeling met verpleging en verzorging](http://purl.org/ozo/onz-zorg#9BVV)
