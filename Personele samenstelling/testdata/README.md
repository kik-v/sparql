# Testdata
## bron
Bevat testdata in een vorm zoals deze opgeslagen zou kunnen zijn in bronsystemen. Hier opgeslagen in de vorm van CSV bestanden, zodat de leesbaarheid optimaal is.
Let op: deze data is in losse bestanden gegenereerd, zonder inhoudelijke consistentie af te dwingen. Een personeelslid zou een contract kunnen hebben van 1-1-2020 t/m 31-12-2020 en een ziekteverzuimperiode in 2018. Dat komt doordat de contracten los van verzuim is gegenereerd.
De data als geheel zal dus niet logisch consistent zijn, maar dat maakt de data nog wel geschikt om de uitkomst van de queries te vergelijken met de gewenste uitkomsten. Het zorg er echter ook voor dat de percentages in query 2.2.1 niet netjes totaliseren naar 100%.
## conversie
Bevat de conversies van de CSV bronbestanden, naar RDF triples, conform de vph-g en vph-zorg ontologieën. Dit json bestand kan rechtstreeks gebruikt worden in Ontorefine, zoals meegeleverd in GraphDB. In de bestanden kan betrekkelijk eenvoudig de RML mapping geïdentificeerd worden voor degene die alleen het RML deel wil gebruiken, los van Ontorefine.
## rdf
Het resultaat van de conversie (volgens bestanden in map 'conversie') van de brondata (in de map 'bron') in turtle formaat.
## resultaat
De geplande uitkomst van de verschillende sparql queries waarin de indicatoren basisveiligheid worden berekend, in de vorm van CSV bestanden.

## Voorbeeld werkwijze
* Implementeer een triple store met OWL-RL reasoning
* Importeer de ontologieën vph-g, vph-pers en vph-zorg
* Importeer alle .ttl bestanden uit de map 'rdf'
* Voer de queries basisveiligheid uit
* Vergelijk de uitkomst met de uitkomsten uit de map 'resultaat'

Voor deze werkwijze zijn de bronbestanden en conversies niet nodig, omdat direct met het resultaat van conversie gewerkt kan worden.

## Compatibilteit
* vph-zorg, development branch na 2022-02-01 (toevoeging zorgprofielen)
* vph-pers, development branch na 2022-01-18 (verplaatsing properties naar vph-g)
* vph-g, develoment branch na 2022-02-17 (werkt waarschijnlijk ook met oudere versies, maar getest met deze versie)