In deze map staat testdata overeenkomstig versie 2 van de ontologie. Deze wijkt af van de 'oude' testdata omdat de 2.x versies niet backwards compatible is met de 1.x versies.

De CSV bron (representatie van bronsystemen) verandert uiteraard niet door een nieuwe ontologie. Deze bron wordt dus opnieuw gebruikt.

Testdata die NIET gewijzigd is en dus ook niet opnieuw in deze mappen opgenomen is:
* Personeel.ttl
