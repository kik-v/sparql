# Indicator: NZA 3.2
# Parameters: $(start_periode), $(eind_periode)
# Ontologie: versie 2.1.1

PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX onz-fin: <http://purl.org/ozo/onz-fin#>
PREFIX onz-pers: <http://purl.org/ozo/onz-pers#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>


SELECT ?teller ?noemer (?teller / ?noemer AS ?indicator)
WHERE 
{
    {
    SELECT ( SUM(?bedrag) AS ?teller ) 
    WHERE 
        {
        BIND("2022-01-01"^^xsd:date AS ?start_periode)
        BIND("2022-12-31"^^xsd:date AS ?eind_periode)

        VALUES ?rubriek { 
            onz-fin:WPerSol
            onz-fin:WPerLes

            onz-fin:PM411000
            onz-fin:PM412000
            onz-fin:PM413000
            onz-fin:PM415000
            onz-fin:PM422100
            onz-fin:PM422300
            onz-fin:PM422400
            onz-fin:PM422410
            onz-fin:PM422500
            onz-fin:PM422600
            onz-fin:PM422900

            # ORT dient niet geincludeerd te worden
            }   

        ?post a onz-fin:Grootboekpost ;
            onz-g:partOf ?rubriek ;
            onz-g:isAbout ?kosten ;
            onz-g:hasDate ?datum .

        ?kosten onz-g:hasQuality / onz-g:hasQualityValue / onz-g:hasDataValue ?bedrag .

        FILTER (?datum >= ?start_periode && ?datum <= ?eind_periode)
        }
    }
    {
    SELECT (SUM(?subtotaal) as ?noemer)
    WHERE 
        {
        SELECT (SUM(?omvang_factor_corr) as ?subtotaal)
        WHERE
        { 
            # definieer periode waarin overeenkomst geldig moet zijn
            BIND("2022-01-01"^^xsd:date AS ?start_periode)
            BIND("2022-12-31"^^xsd:date AS ?eind_periode)

            ?overeenkomst 
                onz-pers:heeftOpdrachtnemer ?persoon ;
                a onz-pers:ArbeidsOvereenkomst ;
                onz-g:hasPart ?omvang ;
                onz-g:startDatum ?start .                
            OPTIONAL {?overeenkomst onz-g:eindDatum ?end}
            # alleen overeenkomsten in de gevraagde periode
            FILTER (?start <= ?eind_periode && (!BOUND(?end) || ?end >= ?start_periode))

            ?omvang
                a onz-pers:ContractOmvang ;
                onz-g:startDatum ?start_omvang ;
                onz-g:isAbout ?omvang_waarde .
            OPTIONAL {?omvang onz-g:eindDatum ?eind_omvang}
            FILTER (?start_omvang <= ?eind_periode && (!BOUND(?eind_omvang) || ?eind_omvang >= ?start_periode))
            ?omvang_waarde
                onz-g:hasDataValue ?omvang_waarde_getal ;
                onz-g:hasUnitOfMeasure ?omvang_waarde_eenheid .
            ?omvang_waarde_eenheid
                onz-pers:hasDenominatorQualityValue onz-g:Week ;
                onz-pers:hasNumeratorQualityValue onz-g:Uur ;
                onz-g:hasDataValue ?omvang_waarde_factor .

            BIND(?omvang_waarde_getal/36*?omvang_waarde_factor AS ?omvang_waarde_rapportage)

            # corrigeer wanneer start of eind van de contractomvang buiten de gevraagde periode valt
            BIND(IF(?start_periode > ?start_omvang, ?start_periode, ?start_omvang) AS ?start_omvang_corr)
            BIND(IF((?eind_periode < ?eind_omvang) || !BOUND(?eind_omvang), ?eind_periode, ?eind_omvang) AS ?eind_omvang_corr)

            # Bereken gevraagd deel van de overeenkomst
            BIND ((360 * (YEAR(?eind_periode) - YEAR(?start_periode))) +
                (30 * (MONTH(?eind_periode) - MONTH(?start_periode))) +
                IF((DAY(?eind_periode) - DAY(?start_periode)) < 29, (DAY(?eind_periode) - DAY(?start_periode)), 29) + 1
                AS ?dagen_periode)
            BIND ((360 * (YEAR(?eind_omvang_corr) - YEAR(?start_omvang_corr))) +
                (30 * (MONTH(?eind_omvang_corr) - MONTH(?start_omvang_corr))) +
                IF((DAY(?eind_omvang_corr) - DAY(?start_omvang_corr)) < 29, (DAY(?eind_omvang_corr) - DAY(?start_omvang_corr)), 29) + 1
                AS ?dagen_overeenkomst)
            BIND (?dagen_overeenkomst/?dagen_periode AS ?factor)
            
            # Bereken relevant deel van de omvangwaarde
            BIND (?factor * ?omvang_waarde_rapportage AS ?omvang_factor_corr)
        } 
        GROUP BY ?overeenkomst ?omvang
        }
    }
}