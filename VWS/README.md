# Indicator 1.1
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Functie in organisatie-rol](http://purl.org/ozo/onz-g#OccupationalPositionRole)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Oproepovereenkomst](http://purl.org/ozo/onz-pers#OproepOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 1.2
## Concepten
- [Mens](http://purl.org/ozo/onz-g#Human)
- [ODB Kwalificatiewaarde](http://purl.org/ozo/onz-pers#ODBKwalificatieWaarde)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 1.3
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Arbeidsovereenkomst bepaalde tijd](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomstBepaaldeTijd)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 1.4
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Oproepovereenkomst](http://purl.org/ozo/onz-pers#OproepOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [heeftBeginTijd](http://purl.org/ozo/onz-g#hasBeginTimeStamp)
- [heeftEindTijd](http://purl.org/ozo/onz-g#hasEndTimeStamp)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 1.5
## Concepten
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Functie in organisatie-rol](http://purl.org/ozo/onz-g#OccupationalPositionRole)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Vrijwilligersovereenkomst](http://purl.org/ozo/onz-pers#VrijwilligersOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 1.6
## Concepten
- [Arbeidsovereenkomst beroeps begeleidende leerweg](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomstBBL)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 1.7
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Verloonde periode](http://purl.org/ozo/onz-fin#VerloondePeriode)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [definieert](http://purl.org/ozo/onz-g#defines)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
# Indicator 2.1
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Onverplaatsbaar artefact](http://purl.org/ozo/onz-g#StationaryArtifact)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft geboortedatum](http://purl.org/ozo/onz-g#hasDateOfBirth)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 3.1
## Concepten
- [Arbeidsongeschiktheid](http://purl.org/ozo/onz-pers#ArbeidsOngeschiktheid)
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Contractomvang](http://purl.org/ozo/onz-pers#ContractOmvang)
- [Onverplaatsbaar artefact](http://purl.org/ozo/onz-g#StationaryArtifact)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Ziekteperiode](http://purl.org/ozo/onz-pers#ZiektePeriode)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deelnemer](http://purl.org/ozo/onz-g#hasParticipant)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [heeft teller kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasNumeratorQualityValue)
- [is eigenschap of kenmerk van](http://purl.org/ozo/onz-g#inheresIn)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 3.2
## Concepten
- [Arbeidsongeschiktheid](http://purl.org/ozo/onz-pers#ArbeidsOngeschiktheid)
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Contractomvang](http://purl.org/ozo/onz-pers#ContractOmvang)
- [Onverplaatsbaar artefact](http://purl.org/ozo/onz-g#StationaryArtifact)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Ziekteperiode](http://purl.org/ozo/onz-pers#ZiektePeriode)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
- [Zwangerschapsverlof](http://purl.org/ozo/onz-pers#ZwangerschapsVerlof)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deelnemer](http://purl.org/ozo/onz-g#hasParticipant)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [heeft teller kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasNumeratorQualityValue)
- [is eigenschap of kenmerk van](http://purl.org/ozo/onz-g#inheresIn)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 3.3
## Concepten
- [Arbeidsongeschiktheid](http://purl.org/ozo/onz-pers#ArbeidsOngeschiktheid)
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Contractomvang](http://purl.org/ozo/onz-pers#ContractOmvang)
- [Onverplaatsbaar artefact](http://purl.org/ozo/onz-g#StationaryArtifact)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Ziekteperiode](http://purl.org/ozo/onz-pers#ZiektePeriode)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deelnemer](http://purl.org/ozo/onz-g#hasParticipant)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [heeft teller kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasNumeratorQualityValue)
- [is eigenschap of kenmerk van](http://purl.org/ozo/onz-g#inheresIn)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 3.4
## Concepten
- [Arbeidsongeschiktheid](http://purl.org/ozo/onz-pers#ArbeidsOngeschiktheid)
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Contractomvang](http://purl.org/ozo/onz-pers#ContractOmvang)
- [Onverplaatsbaar artefact](http://purl.org/ozo/onz-g#StationaryArtifact)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Ziekteperiode](http://purl.org/ozo/onz-pers#ZiektePeriode)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
- [Zwangerschapsverlof](http://purl.org/ozo/onz-pers#ZwangerschapsVerlof)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deelnemer](http://purl.org/ozo/onz-g#hasParticipant)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [heeft teller kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasNumeratorQualityValue)
- [is eigenschap of kenmerk van](http://purl.org/ozo/onz-g#inheresIn)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 4.1
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 4.2
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 4.3
## Concepten
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Opleidingsniveau-waarde](http://purl.org/ozo/onz-g#EducationalLevelValue)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 4.4
## Concepten
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Opleidingsniveau-waarde](http://purl.org/ozo/onz-g#EducationalLevelValue)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 5.1
## Concepten
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Verpleegproces](http://purl.org/ozo/onz-zorg#NursingProcess)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft perdurantlocatie](http://purl.org/ozo/onz-g#hasPerdurantLocation)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [VV Beschermd verblijf met intensieve palliatief-terminale zorg](http://purl.org/ozo/onz-zorg#10VV)
- [VV Beschermd wonen met intensieve dementiezorg](http://purl.org/ozo/onz-zorg#5VV)
- [VV Beschermd wonen met intensieve verzorging en verpleging](http://purl.org/ozo/onz-zorg#6VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding](http://purl.org/ozo/onz-zorg#7VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging](http://purl.org/ozo/onz-zorg#8VV)
- [VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging](http://purl.org/ozo/onz-zorg#4VV)
- [VV Herstelgerichte behandeling met verpleging en verzorging](http://purl.org/ozo/onz-zorg#9BVV)
# Indicator 5.2
## Concepten
- [Leveringsvorm](http://purl.org/ozo/onz-zorg#Leveringsvorm)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Verpleegproces](http://purl.org/ozo/onz-zorg#NursingProcess)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft perdurantlocatie](http://purl.org/ozo/onz-g#hasPerdurantLocation)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [indicatie behandeling](http://purl.org/ozo/onz-zorg#heeftIndicatieBehandeling)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [VV Beschermd verblijf met intensieve palliatief-terminale zorg](http://purl.org/ozo/onz-zorg#10VV)
- [VV Beschermd wonen met intensieve dementiezorg](http://purl.org/ozo/onz-zorg#5VV)
- [VV Beschermd wonen met intensieve verzorging en verpleging](http://purl.org/ozo/onz-zorg#6VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding](http://purl.org/ozo/onz-zorg#7VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging](http://purl.org/ozo/onz-zorg#8VV)
- [VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging](http://purl.org/ozo/onz-zorg#4VV)
- [VV Herstelgerichte behandeling met verpleging en verzorging](http://purl.org/ozo/onz-zorg#9BVV)
- [Verblijf in een instelling](http://purl.org/ozo/onz-zorg#instelling)
# Indicator 7.1
## Concepten
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Wooneenheid](http://purl.org/ozo/onz-org#WoonEenheid)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
# Indicator 7.2
## Concepten
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Capaciteitswaarde](http://purl.org/ozo/onz-g#CapacityValue)
- [Kwaliteit](http://purl.org/ozo/onz-g#Quality)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Wooneenheid](http://purl.org/ozo/onz-org#WoonEenheid)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [heeft capaciteitsobject](http://purl.org/ozo/onz-g#hasCapacityObject)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
