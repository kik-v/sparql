# Indicator 1.1
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Functie in organisatie-rol](http://purl.org/ozo/onz-g#OccupationalPositionRole)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Oproepovereenkomst](http://purl.org/ozo/onz-pers#OproepOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 1.2
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Functie in organisatie-rol](http://purl.org/ozo/onz-g#OccupationalPositionRole)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Oproepovereenkomst](http://purl.org/ozo/onz-pers#OproepOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 2.1
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Gewerkte periode](http://purl.org/ozo/onz-pers#GewerktePeriode)
- [Gewerkte tijd](http://purl.org/ozo/onz-pers#GewerkteTijd)
- [Onverplaatsbaar artefact](http://purl.org/ozo/onz-g#StationaryArtifact)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft perdurantlocatie](http://purl.org/ozo/onz-g#hasPerdurantLocation)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [heeftBeginTijd](http://purl.org/ozo/onz-g#hasBeginTimeStamp)
- [heeftEindTijd](http://purl.org/ozo/onz-g#hasEndTimeStamp)
# Indicator 2.2
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Verloonde periode](http://purl.org/ozo/onz-fin#VerloondePeriode)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [definieert](http://purl.org/ozo/onz-g#defines)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
# Indicator 3.1
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Arbeidsovereenkomst bepaalde tijd](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomstBepaaldeTijd)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 4.1
## Concepten
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Contractomvang](http://purl.org/ozo/onz-pers#ContractOmvang)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Werkovereenkomst](http://purl.org/ozo/onz-pers#WerkOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
- [heeft teller kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasNumeratorQualityValue)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 5.1
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Onverplaatsbaar artefact](http://purl.org/ozo/onz-g#StationaryArtifact)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft geboortedatum](http://purl.org/ozo/onz-g#hasDateOfBirth)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 6.1
## Concepten
- [Gewerkte periode](http://purl.org/ozo/onz-pers#GewerktePeriode)
- [ODB Kwalificatiewaarde](http://purl.org/ozo/onz-pers#ODBKwalificatieWaarde)
- [Rol](http://purl.org/ozo/onz-g#Role)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [heeft perdurantlocatie](http://purl.org/ozo/onz-g#hasPerdurantLocation)
## Eigenschappen
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [heeftBeginTijd](http://purl.org/ozo/onz-g#hasBeginTimeStamp)
- [heeftEindTijd](http://purl.org/ozo/onz-g#hasEndTimeStamp)
# Indicator 7.1
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Gewerkte periode](http://purl.org/ozo/onz-pers#GewerktePeriode)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Verpleegproces](http://purl.org/ozo/onz-zorg#NursingProcess)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft perdurantlocatie](http://purl.org/ozo/onz-g#hasPerdurantLocation)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [heeftBeginTijd](http://purl.org/ozo/onz-g#hasBeginTimeStamp)
- [heeftEindTijd](http://purl.org/ozo/onz-g#hasEndTimeStamp)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [VV Beschermd verblijf met intensieve palliatief-terminale zorg](http://purl.org/ozo/onz-zorg#10VV)
- [VV Beschermd wonen met intensieve dementiezorg](http://purl.org/ozo/onz-zorg#5VV)
- [VV Beschermd wonen met intensieve verzorging en verpleging](http://purl.org/ozo/onz-zorg#6VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding](http://purl.org/ozo/onz-zorg#7VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging](http://purl.org/ozo/onz-zorg#8VV)
- [VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging](http://purl.org/ozo/onz-zorg#4VV)
- [VV Herstelgerichte behandeling met verpleging en verzorging](http://purl.org/ozo/onz-zorg#9BVV)
# Indicator 8.1
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Oproepovereenkomst](http://purl.org/ozo/onz-pers#OproepOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [heeftBeginTijd](http://purl.org/ozo/onz-g#hasBeginTimeStamp)
- [heeftEindTijd](http://purl.org/ozo/onz-g#hasEndTimeStamp)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 8.2
## Concepten
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Financiële entiteit](http://purl.org/ozo/onz-g#FinancialEntity)
- [Grootboekpost](http://purl.org/ozo/onz-fin#Grootboekpost)
- [Kostenplaats](http://purl.org/ozo/onz-fin#Kostenplaats)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [heeft datum](http://purl.org/ozo/onz-g#hasDate)
## Instanties
- [Aandeel werkgever premies sociale verzekeringen](http://purl.org/ozo/onz-fin#PM422400)
- [Aandeel werknemer premies sociale verzekeringen](http://purl.org/ozo/onz-fin#PM422300)
- [Berekende sociale kosten vakantiebijslag](http://purl.org/ozo/onz-fin#PM422100)
- [Ingehuurde ZZP-ers](http://purl.org/ozo/onz-fin#WBedOvpZzp)
- [Ingehuurde payrollers](http://purl.org/ozo/onz-fin#WBedOvpPay)
- [Korting / vrijlating basispremie WAO / werkgeverdeel Awf](http://purl.org/ozo/onz-fin#PM422410)
- [Lonen en salarissen](http://purl.org/ozo/onz-fin#WPerLes)
- [Management fee](http://purl.org/ozo/onz-fin#WBedOvpMaf)
- [Overgeboekte sociale kosten](http://purl.org/ozo/onz-fin#PM422900)
- [Overig ingeleend personeel](http://purl.org/ozo/onz-fin#WBedOvpOip)
- [Overig personeel niet in loondienst](http://purl.org/ozo/onz-fin#PM418200)
- [Pensioenkosten](http://purl.org/ozo/onz-fin#PM422600)
- [Personeel algemene en administratieve functies](http://purl.org/ozo/onz-fin#PM411000)
- [Personeel hotelfuncties](http://purl.org/ozo/onz-fin#PM412000)
- [Personeel patiënt- c.q. bewonergebonden functies](http://purl.org/ozo/onz-fin#PM413000)
- [Personeel terrein- en gebouwgebonden functies](http://purl.org/ozo/onz-fin#PM415000)
- [Sociale lasten](http://purl.org/ozo/onz-fin#WPerSol)
- [Uitzendbedrijven](http://purl.org/ozo/onz-fin#WBedOvpUit)
- [Uitzendkrachten](http://purl.org/ozo/onz-fin#WBedOvpUik)
- [Uitzendkrachten](http://purl.org/ozo/onz-fin#WBedOvpUikUik)
- [Uitzendkrachten](http://purl.org/ozo/onz-fin#PM418100)
- [Uitzendkrachten  formatief](http://purl.org/ozo/onz-fin#WBedOvpUikFor)
- [Uitzendkrachten boven formatief](http://purl.org/ozo/onz-fin#WBedOvpUikBfo)
- [Uitzendkrachten programma's](http://purl.org/ozo/onz-fin#WBedOvpUikPro)
- [Uitzendkrachten projectmatig](http://purl.org/ozo/onz-fin#WBedOvpUikPrj)
- [Vergoedingen voor niet in loondienst verrichte arbeid](http://purl.org/ozo/onz-fin#PM418000)
- [Ziektekostenverzekering](http://purl.org/ozo/onz-fin#PM422500)
# Indicator 9.1
## Concepten
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Functie in organisatie-rol](http://purl.org/ozo/onz-g#OccupationalPositionRole)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Vrijwilligersovereenkomst](http://purl.org/ozo/onz-pers#VrijwilligersOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 10.1
## Concepten
- [Arbeidsovereenkomst beroeps begeleidende leerweg](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomstBBL)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 11.1
## Concepten
- [Arbeidsongeschiktheid](http://purl.org/ozo/onz-pers#ArbeidsOngeschiktheid)
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Contractomvang](http://purl.org/ozo/onz-pers#ContractOmvang)
- [Onverplaatsbaar artefact](http://purl.org/ozo/onz-g#StationaryArtifact)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Ziekteperiode](http://purl.org/ozo/onz-pers#ZiektePeriode)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deelnemer](http://purl.org/ozo/onz-g#hasParticipant)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [heeft teller kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasNumeratorQualityValue)
- [is eigenschap of kenmerk van](http://purl.org/ozo/onz-g#inheresIn)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 11.2
## Concepten
- [Arbeidsongeschiktheid](http://purl.org/ozo/onz-pers#ArbeidsOngeschiktheid)
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Contractomvang](http://purl.org/ozo/onz-pers#ContractOmvang)
- [Onverplaatsbaar artefact](http://purl.org/ozo/onz-g#StationaryArtifact)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Ziekteperiode](http://purl.org/ozo/onz-pers#ZiektePeriode)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
- [Zwangerschapsverlof](http://purl.org/ozo/onz-pers#ZwangerschapsVerlof)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deelnemer](http://purl.org/ozo/onz-g#hasParticipant)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [heeft teller kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasNumeratorQualityValue)
- [is eigenschap of kenmerk van](http://purl.org/ozo/onz-g#inheresIn)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 11.3
## Concepten
- [Arbeidsongeschiktheid](http://purl.org/ozo/onz-pers#ArbeidsOngeschiktheid)
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Contractomvang](http://purl.org/ozo/onz-pers#ContractOmvang)
- [Onverplaatsbaar artefact](http://purl.org/ozo/onz-g#StationaryArtifact)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Ziekteperiode](http://purl.org/ozo/onz-pers#ZiektePeriode)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deelnemer](http://purl.org/ozo/onz-g#hasParticipant)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [heeft teller kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasNumeratorQualityValue)
- [is eigenschap of kenmerk van](http://purl.org/ozo/onz-g#inheresIn)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 11.4
## Concepten
- [Arbeidsongeschiktheid](http://purl.org/ozo/onz-pers#ArbeidsOngeschiktheid)
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Contractomvang](http://purl.org/ozo/onz-pers#ContractOmvang)
- [Onverplaatsbaar artefact](http://purl.org/ozo/onz-g#StationaryArtifact)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Ziekteperiode](http://purl.org/ozo/onz-pers#ZiektePeriode)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
- [Zwangerschapsverlof](http://purl.org/ozo/onz-pers#ZwangerschapsVerlof)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deelnemer](http://purl.org/ozo/onz-g#hasParticipant)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [heeft teller kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasNumeratorQualityValue)
- [is eigenschap of kenmerk van](http://purl.org/ozo/onz-g#inheresIn)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 12.1
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Ziekteperiode](http://purl.org/ozo/onz-pers#ZiektePeriode)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deelnemer](http://purl.org/ozo/onz-g#hasParticipant)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 12.2
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Ziekteperiode](http://purl.org/ozo/onz-pers#ZiektePeriode)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
- [Zwangerschapsverlof](http://purl.org/ozo/onz-pers#ZwangerschapsVerlof)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deelnemer](http://purl.org/ozo/onz-g#hasParticipant)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 13.1
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 13.2
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 13.3
## Concepten
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Opleidingsniveau-waarde](http://purl.org/ozo/onz-g#EducationalLevelValue)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 13.4
## Concepten
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Opleidingsniveau-waarde](http://purl.org/ozo/onz-g#EducationalLevelValue)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 14.1
## Concepten
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Verpleegproces](http://purl.org/ozo/onz-zorg#NursingProcess)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft perdurantlocatie](http://purl.org/ozo/onz-g#hasPerdurantLocation)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [VV Beschermd verblijf met intensieve palliatief-terminale zorg](http://purl.org/ozo/onz-zorg#10VV)
- [VV Beschermd wonen met intensieve dementiezorg](http://purl.org/ozo/onz-zorg#5VV)
- [VV Beschermd wonen met intensieve verzorging en verpleging](http://purl.org/ozo/onz-zorg#6VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding](http://purl.org/ozo/onz-zorg#7VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging](http://purl.org/ozo/onz-zorg#8VV)
- [VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging](http://purl.org/ozo/onz-zorg#4VV)
- [VV Herstelgerichte behandeling met verpleging en verzorging](http://purl.org/ozo/onz-zorg#9BVV)
# Indicator 14.2
## Concepten
- [Leveringsvorm](http://purl.org/ozo/onz-zorg#Leveringsvorm)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Verpleegproces](http://purl.org/ozo/onz-zorg#NursingProcess)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft perdurantlocatie](http://purl.org/ozo/onz-g#hasPerdurantLocation)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [indicatie behandeling](http://purl.org/ozo/onz-zorg#heeftIndicatieBehandeling)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [VV Beschermd verblijf met intensieve palliatief-terminale zorg](http://purl.org/ozo/onz-zorg#10VV)
- [VV Beschermd wonen met intensieve dementiezorg](http://purl.org/ozo/onz-zorg#5VV)
- [VV Beschermd wonen met intensieve verzorging en verpleging](http://purl.org/ozo/onz-zorg#6VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding](http://purl.org/ozo/onz-zorg#7VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging](http://purl.org/ozo/onz-zorg#8VV)
- [VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging](http://purl.org/ozo/onz-zorg#4VV)
- [VV Herstelgerichte behandeling met verpleging en verzorging](http://purl.org/ozo/onz-zorg#9BVV)
- [Verblijf in een instelling](http://purl.org/ozo/onz-zorg#instelling)
# Indicator 14.3
## Concepten
- [Leveringsvorm](http://purl.org/ozo/onz-zorg#Leveringsvorm)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Verpleegproces](http://purl.org/ozo/onz-zorg#NursingProcess)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [indicatie behandeling](http://purl.org/ozo/onz-zorg#heeftIndicatieBehandeling)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Modulair pakket thuis](http://purl.org/ozo/onz-zorg#mpt)
- [Persoonsgebonden budget](http://purl.org/ozo/onz-zorg#pgb)
- [VV Beschermd verblijf met intensieve palliatief-terminale zorg](http://purl.org/ozo/onz-zorg#10VV)
- [VV Beschermd wonen met intensieve dementiezorg](http://purl.org/ozo/onz-zorg#5VV)
- [VV Beschermd wonen met intensieve verzorging en verpleging](http://purl.org/ozo/onz-zorg#6VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding](http://purl.org/ozo/onz-zorg#7VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging](http://purl.org/ozo/onz-zorg#8VV)
- [VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging](http://purl.org/ozo/onz-zorg#4VV)
- [VV Herstelgerichte behandeling met verpleging en verzorging](http://purl.org/ozo/onz-zorg#9BVV)
- [Verblijf in een instelling](http://purl.org/ozo/onz-zorg#instelling)
- [Volledig pakket thuis](http://purl.org/ozo/onz-zorg#vpt)
