PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX onz-pers: <http://purl.org/ozo/onz-pers#>
PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX onz-org: <http://purl.org/ozo/onz-org#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT 
    ?vestiging
    ?zorg
	((ROUND(100*SUM(?ptf * (?totaal_ziektedagen - ?correctie_totaal)))/100) AS ?teller)
    ((ROUND(100*SUM(?ptf * ?dagen_overeenkomst))/100) AS ?noemer)
    ((ROUND(100*SUM(?ptf * (?totaal_ziektedagen - ?correctie_totaal)) / SUM(?ptf * ?dagen_overeenkomst) * 100)/100) AS ?indicator)
{
    {
        SELECT DISTINCT ?vestiging ?zorg ?persoon ?ptf (MAX(?dagen_ziekte) AS ?totaal_ziektedagen) (SUM(?correctie_dagen_ziekte * (1 - (?aopercentage_corr / 100))) AS ?correctie_totaal) ?dagen_overeenkomst
        {
            {
                SELECT DISTINCT ?vestiging ?zorg ?persoon ?omvang ?ptf ?dagen_ziekte ?correctie_dagen_ziekte ?aopercentage_corr ?dagen_overeenkomst
                {
                    BIND("2022-01-01"^^xsd:date AS ?start_periode)
                    BIND("2022-12-31"^^xsd:date AS ?eind_periode)
                    VALUES ?type_verzuim #Keuze in-/exclusief zwangerschapsverlog
                    { 
                        onz-pers:ZiektePeriode
                        #onz-pers:ZwangerschapsVerlof
                    }
                    ?overeenkomst 
                        a onz-pers:ArbeidsOvereenkomst ;
                        onz-pers:heeftOpdrachtnemer ?persoon ;
                        onz-g:isAbout ?locatie ;
                        onz-g:startDatum ?start_overeenkomst .
                        OPTIONAL { ?overeenkomst onz-g:eindDatum ?eind_overeenkomst }
                        FILTER(?start_overeenkomst <= ?eind_periode && ((?eind_overeenkomst >= ?start_periode) || (!BOUND(?eind_overeenkomst))))
                        BIND(IF(?start_overeenkomst <= ?start_periode, ?start_periode, ?start_overeenkomst) AS ?start_overeenkomst_corr)
                        BIND(IF(?eind_overeenkomst >= ?eind_periode || !BOUND(?eind_overeenkomst), ?eind_periode, ?eind_overeenkomst) AS ?eind_overeenkomst_corr)
                    ?locatie 
                        a onz-g:StationaryArtifact ;
                        onz-g:partOf* ?v .
                    ?v 
                        a onz-org:Vestiging ;
                        rdfs:label ?vestiging .

                    OPTIONAL 
                    {
                        ?overeenkomst onz-g:isAbout ?zorgfunctie .
                        ?zorgfunctie a onz-pers:ZorgverlenerFunctie
                    }
                    BIND(IF(BOUND(?zorgfunctie),"Zorg","Niet-zorg") AS ?zorg)
                    ?omvang
                        a onz-pers:ContractOmvang ;
                        onz-g:partOf ?overeenkomst ;
                        onz-g:isAbout ?omvang_waarde ;
                        onz-g:startDatum ?start_omvang .
                        OPTIONAL { ?omvang onz-g:eindDatum ?eind_omvang }
                        FILTER(?start_omvang <= ?eind_overeenkomst_corr && ((?eind_omvang >= ?start_overeenkomst_corr) || (!BOUND(?eind_omvang))))
                        BIND(IF(?start_omvang <= ?start_periode, ?start_periode, ?start_omvang) AS ?start_omvang_corr)
                        BIND(IF(?eind_omvang >= ?eind_periode || !BOUND(?eind_omvang), ?eind_periode, ?eind_omvang) AS ?eind_omvang_corr)
                    #Bereken aantal dagen in overeenkomst
                        BIND ((360 * (YEAR(?eind_omvang_corr) - YEAR(?start_omvang_corr))) +
                            (30 * (MONTH(?eind_omvang_corr) - MONTH(?start_omvang_corr))) +
                            IF((DAY(?eind_omvang_corr) - DAY(?start_omvang_corr)) < 29, (DAY(?eind_omvang_corr) -
                            DAY(?start_omvang_corr)), 29) + 1
                            AS ?dagen_overeenkomst)

                    ?omvang_waarde
                        onz-g:hasDataValue ?omvang_waarde_getal ;
                        onz-g:hasUnitOfMeasure ?omvang_waarde_eenheid .
                    ?omvang_waarde_eenheid
                        onz-pers:hasDenominatorQualityValue onz-g:Week ;
                        onz-pers:hasNumeratorQualityValue onz-g:Uur ;
                        onz-g:hasDataValue ?omvang_waarde_factor .
                        BIND(?omvang_waarde_getal/36*?omvang_waarde_factor AS ?ptf)
                    ?ziekteperiode 
                        a ?type_verzuim ;
                        onz-g:hasParticipant ?persoon ;
                        onz-g:startDatum ?start_ziekte .
                        OPTIONAL {?ziekteperiode onz-g:eindDatum ?eind_ziekte}
                        FILTER(?start_ziekte <= ?eind_omvang_corr && ((?eind_ziekte >= ?start_omvang_corr) || (!BOUND(?eind_ziekte))))

                        #Filter lang/kortdurend verzuim
                        BIND ((360 * (YEAR(?eind_ziekte) - YEAR(?start_ziekte))) +
                                            (30 * (MONTH(?eind_ziekte) - MONTH(?start_ziekte))) +
                                            IF((DAY(?eind_ziekte) - DAY(?start_ziekte)) < 29, (DAY(?eind_ziekte) -
                                            DAY(?start_ziekte)), 29) + 1
                                            AS ?dagen_ziek)
                        FILTER (?dagen_ziek < 29) #kortdurend verzuim
                        #FILTER (?dagen_ziek >= 29 || !BOUND(?dagen_ziek)) #landurend verzuim

                        BIND(IF(?start_ziekte <= ?start_omvang_corr, ?start_omvang_corr, ?start_ziekte) AS ?start_ziekte_corr)
                        BIND(IF(?eind_ziekte >= ?eind_omvang_corr || !BOUND(?eind_ziekte), ?eind_omvang_corr, ?eind_ziekte) AS ?eind_ziekte_corr)
                        OPTIONAL
                        {
                            ?ao 
                                onz-g:inheresIn ?persoon ;
                                a onz-pers:ArbeidsOngeschiktheid ;
                                onz-g:hasQualityValue ?aop .
                            ?aop
                                onz-g:hasDataValue ?aopercentage ;
                                onz-g:startDatum ?start_aop .
                                OPTIONAL { ?aop onz-g:eindDatum ?eind_aop }
                                BIND(IF(!BOUND(?eind_aop), ?eind_ziekte_corr, ?eind_aop) AS ?eind_aop_corr)
                                FILTER(?start_aop <= ?eind_ziekte_corr && ?eind_aop_corr >= ?start_ziekte_corr)
                        }
                        BIND(IF(?start_aop <= ?start_ziekte_corr, ?start_ziekte_corr, ?start_aop) AS ?start_aop_corr)
                        BIND(IF(BOUND(?aopercentage), ?aopercentage, 100) AS ?aopercentage_corr)

                        #Bereken aantal dagen ziek in periode
                        BIND ((360 * (YEAR(?eind_ziekte_corr) - YEAR(?start_ziekte_corr))) +
                                                (30 * (MONTH(?eind_ziekte_corr) - MONTH(?start_ziekte_corr))) +
                                                IF((DAY(?eind_ziekte_corr) - DAY(?start_ziekte_corr)) < 29, (DAY(?eind_ziekte_corr) -
                                                DAY(?start_ziekte_corr)), 29) + 1
                                                AS ?dagen_ziekte)
                        #Bereken correcte aantal dagen deels hersteld o.b.v. AO percentage
                        BIND ((360 * (YEAR(?eind_aop_corr) - YEAR(?start_aop_corr))) +
                                                (30 * (MONTH(?eind_aop_corr) - MONTH(?start_aop_corr))) +
                                                IF((DAY(?eind_aop_corr) - DAY(?start_aop_corr)) < 29, (DAY(?eind_aop_corr) -
                                                DAY(?start_aop_corr)), 29) + 1
                                                AS ?dagen_deelshersteld)
                        BIND(IF(BOUND(?dagen_deelshersteld),?dagen_deelshersteld,0) AS ?correctie_dagen_ziekte)
                        
                }
            }
        }
        GROUP BY ?vestiging ?zorg ?persoon ?ptf ?totaal_ziek ?dagen_overeenkomst
    }
}
GROUP BY ?vestiging ?zorg