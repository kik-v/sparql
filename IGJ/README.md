# Indicator 1.1.1
## Concepten
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Verpleegproces](http://purl.org/ozo/onz-zorg#NursingProcess)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft perdurantlocatie](http://purl.org/ozo/onz-g#hasPerdurantLocation)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [VV Beschermd verblijf met intensieve palliatief-terminale zorg](http://purl.org/ozo/onz-zorg#10VV)
- [VV Beschermd wonen met intensieve dementiezorg](http://purl.org/ozo/onz-zorg#5VV)
- [VV Beschermd wonen met intensieve verzorging en verpleging](http://purl.org/ozo/onz-zorg#6VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding](http://purl.org/ozo/onz-zorg#7VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging](http://purl.org/ozo/onz-zorg#8VV)
- [VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging](http://purl.org/ozo/onz-zorg#4VV)
- [VV Herstelgerichte behandeling met verpleging en verzorging](http://purl.org/ozo/onz-zorg#9BVV)
# Indicator 1.1.2
## Concepten
- [Leveringsvorm](http://purl.org/ozo/onz-zorg#Leveringsvorm)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Verpleegproces](http://purl.org/ozo/onz-zorg#NursingProcess)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft perdurantlocatie](http://purl.org/ozo/onz-g#hasPerdurantLocation)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [indicatie behandeling](http://purl.org/ozo/onz-zorg#heeftIndicatieBehandeling)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [VV Beschermd verblijf met intensieve palliatief-terminale zorg](http://purl.org/ozo/onz-zorg#10VV)
- [VV Beschermd wonen met intensieve dementiezorg](http://purl.org/ozo/onz-zorg#5VV)
- [VV Beschermd wonen met intensieve verzorging en verpleging](http://purl.org/ozo/onz-zorg#6VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding](http://purl.org/ozo/onz-zorg#7VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging](http://purl.org/ozo/onz-zorg#8VV)
- [VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging](http://purl.org/ozo/onz-zorg#4VV)
- [VV Herstelgerichte behandeling met verpleging en verzorging](http://purl.org/ozo/onz-zorg#9BVV)
- [Verblijf in een instelling](http://purl.org/ozo/onz-zorg#instelling)
# Indicator 1.1.3
## Concepten
- [Leveringsvorm](http://purl.org/ozo/onz-zorg#Leveringsvorm)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Verpleegproces](http://purl.org/ozo/onz-zorg#NursingProcess)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft perdurantlocatie](http://purl.org/ozo/onz-g#hasPerdurantLocation)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [indicatie behandeling](http://purl.org/ozo/onz-zorg#heeftIndicatieBehandeling)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Modulair pakket thuis](http://purl.org/ozo/onz-zorg#mpt)
- [Persoonsgebonden budget](http://purl.org/ozo/onz-zorg#pgb)
- [VV Beschermd verblijf met intensieve palliatief-terminale zorg](http://purl.org/ozo/onz-zorg#10VV)
- [VV Beschermd wonen met intensieve dementiezorg](http://purl.org/ozo/onz-zorg#5VV)
- [VV Beschermd wonen met intensieve verzorging en verpleging](http://purl.org/ozo/onz-zorg#6VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding](http://purl.org/ozo/onz-zorg#7VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging](http://purl.org/ozo/onz-zorg#8VV)
- [VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging](http://purl.org/ozo/onz-zorg#4VV)
- [VV Herstelgerichte behandeling met verpleging en verzorging](http://purl.org/ozo/onz-zorg#9BVV)
- [Verblijf in een instelling](http://purl.org/ozo/onz-zorg#instelling)
- [Volledig pakket thuis](http://purl.org/ozo/onz-zorg#vpt)
# Indicator 1.1.4
## Concepten
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Verpleegproces](http://purl.org/ozo/onz-zorg#NursingProcess)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft perdurantlocatie](http://purl.org/ozo/onz-g#hasPerdurantLocation)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [VV Beschermd verblijf met intensieve palliatief-terminale zorg](http://purl.org/ozo/onz-zorg#10VV)
- [VV Beschermd wonen met intensieve dementiezorg](http://purl.org/ozo/onz-zorg#5VV)
- [VV Beschermd wonen met intensieve verzorging en verpleging](http://purl.org/ozo/onz-zorg#6VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding](http://purl.org/ozo/onz-zorg#7VV)
- [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging](http://purl.org/ozo/onz-zorg#8VV)
- [VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging](http://purl.org/ozo/onz-zorg#4VV)
- [VV Herstelgerichte behandeling met verpleging en verzorging](http://purl.org/ozo/onz-zorg#9BVV)
# Indicator 1.2.1
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Oproepovereenkomst](http://purl.org/ozo/onz-pers#OproepOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 1.2.2
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Contractomvang](http://purl.org/ozo/onz-pers#ContractOmvang)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Oproepovereenkomst](http://purl.org/ozo/onz-pers#OproepOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [heeft teller kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasNumeratorQualityValue)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 1.2.3
## Concepten
- [Mens](http://purl.org/ozo/onz-g#Human)
- [ODB Kwalificatiewaarde](http://purl.org/ozo/onz-pers#ODBKwalificatieWaarde)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 1.2.4
## Concepten
- [Contractomvang](http://purl.org/ozo/onz-pers#ContractOmvang)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Opleidingsniveau-waarde](http://purl.org/ozo/onz-g#EducationalLevelValue)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [heeft teller kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasNumeratorQualityValue)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 1.2.5
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Opleidingsniveau-waarde](http://purl.org/ozo/onz-g#EducationalLevelValue)
- [Oproepovereenkomst](http://purl.org/ozo/onz-pers#OproepOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Indicator 1.2.6
## Concepten
- [Arbeidsovereenkomst](http://purl.org/ozo/onz-pers#ArbeidsOvereenkomst)
- [Contractomvang](http://purl.org/ozo/onz-pers#ContractOmvang)
- [Inhuurovereenkomst](http://purl.org/ozo/onz-pers#InhuurOvereenkomst)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Opleidingsniveau-waarde](http://purl.org/ozo/onz-g#EducationalLevelValue)
- [Oproepovereenkomst](http://purl.org/ozo/onz-pers#OproepOvereenkomst)
- [Uitzendovereenkomst](http://purl.org/ozo/onz-pers#UitzendOvereenkomst)
- [Zorgverlener (functie)](http://purl.org/ozo/onz-pers#ZorgverlenerFunctie)
## Relaties
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft eigenschap of kenmerk](http://purl.org/ozo/onz-g#hasQuality)
- [heeft eigenschapswaarde](http://purl.org/ozo/onz-g#hasQualityValue)
- [heeft meeteenheid](http://purl.org/ozo/onz-g#hasUnitOfMeasure)
- [heeft noemer kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasDenominatorQualityValue)
- [heeft opdrachtnemer](http://purl.org/ozo/onz-pers#heeftOpdrachtnemer)
- [heeft teller kwaliteitswaarde](http://purl.org/ozo/onz-pers#hasNumeratorQualityValue)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [heeft datawaarde](http://purl.org/ozo/onz-g#hasDataValue)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Uur](http://purl.org/ozo/onz-g#Uur)
- [Week](http://purl.org/ozo/onz-g#Week)
# Indicator 1.3.1
## Concepten
- [Gewerkte periode](http://purl.org/ozo/onz-pers#GewerktePeriode)
## Eigenschappen
- [heeftBeginTijd](http://purl.org/ozo/onz-g#hasBeginTimeStamp)
- [heeftEindTijd](http://purl.org/ozo/onz-g#hasEndTimeStamp)
