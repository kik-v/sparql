# Verdeelsleutel financieringsstroom
## Concepten
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Verpleegproces](http://purl.org/ozo/onz-zorg#NursingProcess)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Wlz-indicatie](http://purl.org/ozo/onz-zorg#WlzIndicatie)
- [Wmo-indicatie](http://purl.org/ozo/onz-zorg#WmoIndicatie)
- [Zvw-indicatie](http://purl.org/ozo/onz-zorg#ZvwIndicatie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft perdurantlocatie](http://purl.org/ozo/onz-g#hasPerdurantLocation)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
# Verdeelsleutel sector langdurige zorg
## Concepten
- [Bedrijf](http://purl.org/ozo/onz-g#Business)
- [Langdurige zorg sector](http://purl.org/ozo/onz-zorg#LangdurigeZorgSector)
- [Mens](http://purl.org/ozo/onz-g#Human)
- [Verpleegproces](http://purl.org/ozo/onz-zorg#NursingProcess)
- [Vestiging](http://purl.org/ozo/onz-org#Vestiging)
- [Wlz-indicatie](http://purl.org/ozo/onz-zorg#WlzIndicatie)
## Relaties
- [deel van](http://purl.org/ozo/onz-g#partOf)
- [gaat over](http://purl.org/ozo/onz-g#isAbout)
- [gedefinieerd door](http://purl.org/ozo/onz-g#definedBy)
- [heeft deel](http://purl.org/ozo/onz-g#hasPart)
- [heeft perdurantlocatie](http://purl.org/ozo/onz-g#hasPerdurantLocation)
- [vestiging van](http://purl.org/ozo/onz-org#vestigingVan)
## Eigenschappen
- [einddatum](http://purl.org/ozo/onz-g#eindDatum)
- [startdatum](http://purl.org/ozo/onz-g#startDatum)
## Instanties
- [Sector GGZ, B-groep](http://purl.org/ozo/onz-zorg#GGZ-B)
- [Sector Lichamelijk gehandicapt](http://purl.org/ozo/onz-zorg#LG)
- [Sector Licht verstandelijk gehandicapt](http://purl.org/ozo/onz-zorg#LVG)
- [Sector Verpleging en verzorging](http://purl.org/ozo/onz-zorg#VV)
- [Sector Verstandelijk gehandicapt](http://purl.org/ozo/onz-zorg#VG)
- [Sector Zintuiglijk gehandicapt, auditief en communicatief](http://purl.org/ozo/onz-zorg#ZGAUD)
- [Sector Zintuiglijk gehandicapt, visueel](http://purl.org/ozo/onz-zorg#ZGVIS)
