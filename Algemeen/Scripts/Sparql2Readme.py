import re
import os
import rdflib 
from rdflib import RDF, OWL, SKOS, RDFS, XSD
from rdflib.term import URIRef
from gitlab_scripts import get_onto_from_gitlab

ignore_prefixes = ['rdf', 'rdfs', 'xsd']
SPARQL_characters = ['/', '.', ',', '^', '<', '>', '{', '}', ';', ' ']
onto = rdflib.Graph()
branch_name = 'development'

def getNumberFromString(string):
    #werkt met nummers met max 3 componenten met punten gescheiden en getallen kleiner dan 1000
    reString = '(\d+)\.*(\d*)\.*(\d*)'
    x = re.search(reString, string)
    sortNumber = 0
    if x:
        withDots = (x.group().strip('.'))
        list = withDots.split('.')
        factor = 1000
        for number in list:
            if len(number) > 3:
                return 0
            factor = factor / 1000
            sortNumber += int(number) * factor
    return sortNumber

def getQueryList(source):
    query_list = {}
    file_list = os.listdir(source)
    file_list.sort(key = getNumberFromString)
    for file in file_list:
        if file[-3:] == '.rq':
            f = open(source + '/' + file, "r")
            query_list[file[:-3]]=f.read()
    return(query_list)

def get_prefixes(query):
    prefixes = {}
    lines = query.split('\n')
    for line in lines:
        stripped_line = line.replace(' ','')
        colon_pos = stripped_line.find(':<')
        if stripped_line[:6] == 'PREFIX' and colon_pos != -1:
            prefix = line.replace(' ','')[6:colon_pos]
            if prefix == '':
                prefix = ' '
            url = line.replace(' ','')[colon_pos + 2 : -1]
            if not prefix in ignore_prefixes:
                prefixes[prefix] = url
    return prefixes

def get_uris(query, prefixes):
    uris = []
    lines = query.split('\n')
    for line in lines:
        stripped_line = line.strip()
        if not (stripped_line[:1] == '#' or stripped_line[:6] == 'PREFIX' or stripped_line[:6] == 'prefix'): #ignore comments
            for prefix in prefixes: #Find URI through prefixes
                prefix_pos = 0
                while prefix_pos < len(stripped_line):
                    prefix_pos = stripped_line.find(prefix + ':', prefix_pos)
                    if prefix_pos == -1: #prefix not found
                        break
                    end_uri_pos = -1
                    for char in SPARQL_characters:
                        foundnew = stripped_line.find(char, prefix_pos)
                        #print(stripped_line, prefix_pos, char, foundnew)
                        if foundnew != -1 and (foundnew < end_uri_pos or end_uri_pos == -1):
                            end_uri_pos = foundnew
                    if end_uri_pos == -1: #space not found, so uri at end of line
                        end_uri_pos = len(stripped_line)
                    uri = stripped_line[prefix_pos + len(prefix) + 1:end_uri_pos]
                    #remove SPARQL characters
                    if uri[-1:] in SPARQL_characters:
                        uri = uri[:-1]
                    if uri != '' and uri != prefixes[prefix] :
                        uris.append(prefixes[prefix] + uri)
                    prefix_pos += len(prefix)
            #Now find URI without prefix
            uri_pos = 0
            while uri_pos < len(stripped_line):
                uri_pos = stripped_line.find('<', uri_pos)
                if uri_pos == -1:
                    break
                uri_end_pos = stripped_line.find('>', uri_pos)
                if uri_end_pos == -1:
                    break
                uri = stripped_line[uri_pos + 1 : uri_end_pos]
                #check for algorithm that contains '<' and '>'
                if not ' ' in uri:
                    uris.append(uri)
                uri_pos = uri_end_pos
    uris = list(dict.fromkeys(uris)) #remove duplicates from uris
    return (uris)

def uriIsConcept(onto, uri):
    concept = rdflib.URIRef(uri)
    if onto.value(concept, RDF.type) == OWL.Class:
        return True
    else:
        return False

def uriIsObjectProperty(onto, uri):
    concept = rdflib.URIRef(uri)
    if onto.value(concept, RDF.type) == OWL.ObjectProperty:
        return True
    else:
        return False

def uriIsDataProperty(onto, uri):
    concept = rdflib.URIRef(uri)
    if onto.value(concept, RDF.type) == OWL.DatatypeProperty:
        return True
    else:
        return False

def uriIsIndividual(onto, uri):
    concept = rdflib.URIRef(uri)
    if onto.value(concept, RDF.type) == OWL.NamedIndividual:
        return True
    else:
        return False

def uriDefinition(onto, uri):
    concept = rdflib.URIRef(uri)
    if (concept, SKOS.definition, None) in onto:
        definition = next(onto.objects(concept, SKOS.definition))
        return definition
    else:
        return 'Geen definitie in ontologie gevonden.'

def uri2md(uri):
    label_list = {}
    if (URIRef(uri), RDFS.label, None) in onto:
        for s, p , label in onto.triples((URIRef(uri), RDFS.label, None)):
            label_list[label.language] = label
        if 'nl' in label_list:
            return ('[' + label_list['nl'] + '](' + uri + ')')
        if 'en' in label_list:
            return ('[' + label_list['en'] + '](' + uri + ')')
        return ('[' + label + '](' + uri + ')')
    elif uri.find('#'):
        l = uri[(uri.find('#')-len(uri)+1):]
        return ('[' + l + '](' + uri + ')')
    else:
        return ('[' + uri + '](' + uri + ')')

def documentQuery(query, concepts, objectProperties, dataProperties, individuals):
    document = ''
    document += '# ' + query + '\n'
    if len(concepts) > 0:
        document += '## Concepten\n'
        for concept in concepts:
            document += '- ' + uri2md(concept) + '\n'
    if len(objectProperties) > 0:
        document += '## Relaties\n'
        for objectProperty in objectProperties:
            document += '- ' + uri2md(objectProperty) + '\n'
    if len(dataProperties) > 0:
        document += '## Eigenschappen\n'
        for dataProperty in dataProperties:
            document += '- ' + uri2md(dataProperty) + '\n'
    if len(individuals) > 0:
        document += '## Instanties\n'
        for individual in individuals:
            document += '- ' + uri2md(individual) + '\n'
    return (document)

def getSortOrder(string):
    reString = '\[(.*)\]'
    line = uri2md(string)
    x = re.search(reString, line)
    if x:
        return x.group().strip('[').strip(']')
    else:
        return 'zzz'

def querylist2md(querylist):
    document = ''
    for query in querylist:
        concepts = []
        objectProperties = []
        dataProperties = []
        individuals = []
        prefixes = get_prefixes(querylist[query])
        uris = get_uris(querylist[query], prefixes)
        for uri in uris:
            if uri[-1:] == '*':
                uri = uri[:len(uri) - 1]
            if uriIsConcept(onto, uri):
                concepts.append(uri)
            elif uriIsObjectProperty(onto, uri):
                objectProperties.append(uri)
            elif uriIsDataProperty(onto, uri):
                dataProperties.append(uri)
            elif uriIsIndividual(onto, uri):
                individuals.append(uri)
                #print ('individual',uri)
            else:
                print('Warming. Ignored uri:', uri)
        concepts.sort(key=getSortOrder)
        objectProperties.sort(key=getSortOrder)
        dataProperties.sort(key=getSortOrder)
        individuals.sort(key=getSortOrder)
        document += documentQuery(query, concepts, objectProperties, dataProperties, individuals)
    return(document)

def getQueryMaps():
    map = os.listdir('.')
    maplist = []
    for item in map:
        contains_query = False
        if os.path.isdir(item):
            files = os.listdir(item)
            for file in files:
                if file[-3:] == '.rq':
                    contains_query = True
        if contains_query:
            maplist.append(item)
    return maplist

onto = get_onto_from_gitlab(branch_name)

for map_with_query in getQueryMaps():
    if map_with_query != 'Templates':
        queries = getQueryList(map_with_query)
        readme = querylist2md(queries)
        f = open(map_with_query + '/README.md', 'w')
        f.write(readme)
        f.close()
        print(map_with_query + '/README.md opgeslagen')